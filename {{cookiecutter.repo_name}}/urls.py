from django.urls import path, include
from django.contrib import admin

from rest_framework import urls as drf_urls

from {{ cookiecutter.repo_name }} import api_urls


urlpatterns = [
    # Examples:
    # url(r'^$', '{{ cookiecutter.project_name }}.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    path('admin/', admin.site.urls),
    path('api/v1/', include(api_urls)),
    path('api-auth', include(drf_urls)),
]
