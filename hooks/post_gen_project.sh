#!/bin/bash
USER="{{ cookiecutter.username }}"
EMAIL="{{ cookiecutter.email }}"
virtualenv -p {{ cookiecutter.python }} venv
source venv/bin/activate
pip install --upgrade pip setuptools
pip install -r requirements.txt
pip freeze --local > requirements.txt
cp environ.py.dist environ.py
python manage.py migrate --noinput
{% if cookiecutter.create_superuser == 'yes' %}
  echo "Creating superuper {{ cookiecutter.username }}"
  python manage.py createsuperuser --username {{ cookiecutter.username }} --email {{ cookiecutter.email }}
{% endif %}
mv gitignore.txt .gitignore
git init .
git add .
git commit -m "{{ cookiecutter.project_name }} initialized with cookiecutter from http://bitbucket.or/levit_scs/cc_project_app_drf"
